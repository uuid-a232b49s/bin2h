bin2h
+++++


What is this?
-------------

- primarily a cmake tool

- naively (and portably) converts a binary file into a c source


How to use this?
----------------

- ``add_subdirectory`` this repo into the project

- ``bin2h_add`` to add the target binary file

  .. code-block :: cmake

    # process 'binaryFile_0' (relative to the calling list file)
    # generated source: ${target_0_C}
    # generated header: ${target_0_H}
    # check cmake function documentation
    # created dependency target: target_0_TARGET
    bin2h_add(IDENTIFIER target_0 INPUT ./binaryFile_0)


Tested compilers
----------------

- GCC 10.2.1

- Clang 11

- MSVC 19
