#include <string.h>
#include <assert.h>

#include "bin2h/target_0/target_0.h"

int main(int argc, char * * argv) {
    const char value[] = "test-0-file";
    assert(target_0_COUNT == (sizeof(value) - 1));
    assert(target_0_COUNT == (target_0_DATA_END - target_0_DATA_BEGIN));
    assert(strcmp(value, target_0_DATA_BEGIN) == 0);
    return 0;
}
