#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "bin2h/licenseText/licenseText.h"

int main(int argc, char * * argv) {
    FILE * l = fopen(argv[1], "r");
    if (l == NULL) { return -1; }

    char buffer[32];
    size_t i = 0;
    size_t nread;
    while ((nread = fread(buffer, sizeof(buffer[0]), sizeof(buffer), l)) > 0) {
        for (size_t j = i; (i - j) < nread; ++i) {
            if (buffer[i - j] != licenseText_DATA_BEGIN[i]) { return 1; }
        }
    }
    if (i != licenseText_COUNT) { return 1; }
    return 0;
}
