#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct bin2h_args {
    FILE * input;
    FILE * output_c;
    FILE * output_h;
    const char * dataElementType;
    const char * identifier;
    size_t align;
    char * buffer;
    size_t bufferSize;
};

static int bin2h(const struct bin2h_args * args) {
#define check_r(errMsg) \
    if (r < 0) {\
        fprintf(stderr, errMsg);\
        return 2;\
    }
    static const char * errMsg_output_c = "failed to write to output_c\n";
    static const char * errMsg_output_h = "failed to write to output_h\n";
    static const char * errMsg_input = "failed to read from input\n";
    int r;

    if (args->output_c != NULL) {
        r = fprintf(args->output_c,
            "#include \"./include/bin2h/%s/%s.h\"\n"
            "#ifdef __cplusplus\n"
            "extern \"C\" {\n"
            "#endif\n"
            "static ",
            args->identifier, args->identifier
        ); check_r(errMsg_output_c);
        if (args->align != 0) {
            fprintf(args->output_c,
                "_Alignas(%llu) ",
                (unsigned long long) args->align
            );
        }
        r = fprintf(args->output_c,
            "%s %s_DATA[] = {\n",
            args->dataElementType, args->identifier
        ); check_r(errMsg_output_c);
        size_t totalSize = 0;
        {
            size_t nread;
            size_t nprinted = 0;
            while ((nread = fread(args->buffer, sizeof(args->buffer[0]), args->bufferSize, args->input)) > 0) {
                r = ferror(args->input); check_r(errMsg_input);
                for (size_t i = 0; i < nread; ++i, ++nprinted) {
                    fprintf(args->output_c, "%hhu,", (unsigned char) args->buffer[i]);
                    if (((nprinted + 1) % 50) == 0) { fprintf(args->output_c, "\n"); }
                }
                totalSize += nread;
                r = ferror(args->output_c); check_r(errMsg_output_c);
            }
        }
        r = fprintf(args->output_c,
            "%hho\n"
            "};\n"
            "const unsigned long long %s_COUNT = %llu;\n"
            "%s * const %s_DATA_BEGIN = %s_DATA;\n"
            "%s * const %s_DATA_END = %s_DATA + %llu;\n"
            "#ifdef __cplusplus\n"
            "}\n"
            "#endif\n",
            (unsigned char) 0,
            args->identifier, (unsigned long long) totalSize,
            args->dataElementType, args->identifier, args->identifier,
            args->dataElementType, args->identifier, args->identifier, (unsigned long long) totalSize
        ); check_r(errMsg_output_c);
        r = fflush(args->output_c); check_r(errMsg_output_c);
    }
    if (args->output_h != NULL) {
        r = fprintf(args->output_h,
            "#ifndef HEADER_BIN2H_%s\n"
            "#define HEADER_BIN2H_%s\n"
            "#ifdef __cplusplus\n"
            "extern \"C\" {\n"
            "#endif\n"
            "extern const unsigned long long %s_COUNT;\n"
            "extern %s * const %s_DATA_BEGIN;\n"
            "extern %s * const %s_DATA_END;\n"
            "#ifdef __cplusplus\n"
            "}\n"
            "#endif\n"
            "#endif\n",
            args->identifier,
            args->identifier,
            args->identifier,
            args->dataElementType, args->identifier,
            args->dataElementType, args->identifier
        ); check_r(errMsg_output_h);
        r = fflush(args->output_h); check_r(errMsg_output_h);
    }
    return 0;
#undef check_r
}

int main(int argc, char * * argv) {
    if (argc != 7) {
        fprintf(stderr, "usage: bin2h <identifier> <element type> <align> <input> <output-c> <output-h>\n");
        return -1;
    }

    int retCode = 1;
    FILE * in = NULL;
    FILE * out_c = NULL;
    FILE * out_h = NULL;

    size_t align_;
    {
        char * arg = argv[3];
        char * e_;
        align_ = strtoull(arg, &e_, 10);
        if (strlen(arg) != (e_ - arg)) {
            fprintf(stderr, "failed to parse align");
            goto lbl_exit;
        }
    }

    char * argInput = argv[4];
    in = (strcmp(argInput, "-") == 0) ? stdin : fopen(argInput, "r");
    if (in == NULL) {
        fprintf(stderr, "failed to open input: %s\n", argInput);
        goto lbl_exit;
    }

    char * argOutC = argv[5];
    out_c = fopen(argOutC, "w");
    if (out_c == NULL) {
        fprintf(stderr, "failed to open output-c: %s\n", argOutC);
        goto lbl_exit;
    }

    char * argOutH = argv[6];
    out_h = fopen(argOutH, "w");
    if (out_h == NULL) {
        fprintf(stderr, "failed to open output-h: %s\n", argOutH);
        goto lbl_exit;
    }

    static char g_buffer[4096];

    retCode = bin2h(&((struct bin2h_args) {
        .input = in,
        .output_c = out_c,
        .output_h = out_h,
        .dataElementType = argv[2],
        .identifier = argv[1],
        .align = align_,
        .buffer = g_buffer,
        .bufferSize = sizeof(g_buffer)
    }));

lbl_exit:;
    if (in != NULL && in != stdin) { fclose(in); }
    if (out_c != NULL) { fclose(out_c); }
    if (out_h != NULL) { fclose(out_h); }
    return retCode;
}
